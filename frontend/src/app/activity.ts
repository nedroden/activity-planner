export class Activity {
    id: number;
    title: string;
    description: string;

    // @todo Replace with Date
    starts_at: string;
    ends_at: string;
}